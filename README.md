# README #

Windows holographic application to dynamically generate a cuboid of arrows, give size 'n'. The application is controlled using voice commands.

### Setup ###

Import the application to unity. Build it for Windows Platform. Open the <Application>.sln file in Visual Studio and run it on a remote server by specifying the IP address of hololens.

### Guidelines ###

The application is controlled using speech commands.
* Move: Rotates the arrows 
* Translate: Moves the arrows in a circle
* Fall: Adds gravity to arrows and makes them fall
* Generate: Regenerates the arrows at their original position

### Implementation ###

* Assets/genArrow.cs: The Main controller script used to generate Arrows and update frames depending on action invoked.
* Assets/ArrowCollection.cs : Class to contain array of arrows and implementation of rotate, translate, generate and fall methods.
* Assets/SpeechRecognizer.cs : Script to recognize voice commands and invoke actions
* Assets/FrameRate.cs : Script to display and update frame rate for the application