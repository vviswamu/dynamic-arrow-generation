﻿using UnityEngine;
using System.Collections;
using System;
using SpeechRecognizerHelper;

namespace ArrowCollectionHelper 
{
    public class ArrowCollection : MonoBehaviour
    {
        int n;
        GameObject[,,] arrowArray;
        int m;
        //const float distance = 1f;
        //const float scale = 0.05f;

        const float distance = 0.05f;
        const float scale = 0.02f;

        public ArrowCollection(int n)
       {
            this.n = n;
            m = n / 2;
            arrowArray = new GameObject[n,n,n];
            start();
       }

       public int getN()
        {
            return n;
        }

       public void start()
        {
            for (var i = 0; i < n; i++)
            {
                for (var j = 0; j < n; j++)
                {
                    for (var k = 0; k < n; k++)
                    {

                        //arrowArray[i, j, k] = (GameObject)Instantiate(Resources.Load("bowandarrow/Prefabs/arrow"));
                        //arrowArray[i, j, k] = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                        arrowArray[i, j, k] = GameObject.CreatePrimitive(PrimitiveType.Cube);
                        Renderer r = arrowArray[i, j, k].GetComponent<Renderer>();
                        r.material = (Material)Resources.Load("Materials/Red");
                        arrowArray[i, j, k].transform.position = new Vector3((i - m) * distance, ((j - m) * distance), ((k) * distance) + 1.3f);
                        //arrowArray[i, j, k].transform.position = new Vector3((i - m) * distance, ((j - m) * distance), ((k-m) * distance) );
                        arrowArray[i, j, k].transform.localRotation = new Quaternion(0, 0, 0, 0);
                        arrowArray[i, j, k].transform.localScale = new Vector3(1*scale,1*scale,1*scale);

                        

                    }
                }
            }
            genArrow.mode = (int)genArrow.MODES.Stop;
        }

        public void location(Vector3 initialPosition)
        {
            for (var i = 0; i < n; i++)
            {
                for (var j = 0; j < n; j++)
                {
                    for (var k = 0; k < n; k++)
                    {

                        
                        arrowArray[i, j, k].transform.position = new Vector3((initialPosition.x + i - m) * distance, ((initialPosition.y + j - m) * distance), ((initialPosition.z + k) * distance) + 1.3f);
                        //arrowArray[i, j, k].transform.position = new Vector3((initialPosition.x + i - m) * distance, ((initialPosition.y + j - m) * distance), ((initialPosition.z + k - m) * distance));


                    }
                }
            }

        }

        public void rotate(float frameRate)
        {
            for (var i = 0; i < n; i++)
            {
                for (var j = 0; j < n; j++)
                {

                    for (var k = 0; k < n; k++)
                    {

                        arrowArray[i, j, k].transform.Rotate(0, (360 / frameRate), 0);

                    }
                }
            }
        }


        public void translate(int l)
        {

            var radius = 0.01f;

            for (var i = 0; i < n; i++)
            {
                for (var j = 0; j < n; j++)
                {
                    for (var k = 0; k < n; k++)
                    {
                        arrowArray[i, j, k].transform.position += getNextPosition(l * 6, radius, new Vector3(0, 0, 0));
                        arrowArray[i, j, k].transform.Rotate(0, 6, 0);
                    }
                }
            }

        }

        Vector3 getNextPosition(float angle, float radius, Vector3 center)
        {

            Vector3 resultPosition = new Vector3();
            resultPosition.x = center.x + radius * Mathf.Sin(angle * Mathf.Deg2Rad);
            resultPosition.z = center.z + radius * Mathf.Cos(angle * Mathf.Deg2Rad);
            resultPosition.y = center.y;
            return resultPosition;
        }

        public void Generate(Vector3 initialPosition)
        {
            float[,,] lengths = new float[n,n,n];
            System.Random r = new System.Random();

            for (var i = 0; i < n; i++)
            {
                for (var j = 0; j < n; j++)
                {

                    for (var k = 0; k < n; k++)
                    {
                        Rigidbody gameObjectsRigidBody = arrowArray[i, j, k].GetComponent<Rigidbody>();
                        Destroy(gameObjectsRigidBody);
                        arrowArray[i, j, k].transform.position = new Vector3((initialPosition.x + i - m) * distance, (initialPosition.y + j - m) * distance, ((initialPosition.z + k - m) * distance));
                        //arrowArray[i, j, k].transform.localScale = new Vector3(1*scale, 1*scale, 1*scale);
                        arrowArray[i, j, k].transform.localScale = new Vector3(r.Next(1,10) * scale, r.Next(1,10) * scale, r.Next(1,10) * scale);
                        arrowArray[i, j, k].transform.Rotate(r.Next(-360,360), r.Next(-360,360), r.Next(-360,360));
                        
                    }
                }
            }
            
        }

        public void Fall()
        {
            for (var i = 0; i < n; i++)
            {
                for (var j = 0; j < n; j++)
                {

                    for (var k = 0; k < n; k++)
                    {

                        Rigidbody gameObjectsRigidBody = arrowArray[i, j, k].AddComponent<Rigidbody>();

                    }
                }
            }
 
        }

        public void destroy()
        {
            for (var i = 0; i < n; i++)
            {
                for (var j = 0; j < n; j++)
                {

                    for (var k = 0; k < n; k++)
                    {

                        Destroy(arrowArray[i, j, k]);

                    }
                }
            }

        }

    }
}
