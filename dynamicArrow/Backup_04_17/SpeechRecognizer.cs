﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine.Windows.Speech;

namespace SpeechRecognizerHelper
{
    public class SpeechRecognizer
    {
        KeywordRecognizer keywordRecognizer = null;
        Dictionary<string, System.Action> keywords = new Dictionary<string, System.Action>();
        public enum MODES { Rotate, Translate, Generate, Fall, Stop, Increment, Increase };
        int mode;
       

        public SpeechRecognizer()
        {
            mode = (int)MODES.Stop;
            //mode = (int)MODES.Generate;
            addKeywords();
            keywordRecognizer = new KeywordRecognizer(keywords.Keys.ToArray());
            keywordRecognizer.OnPhraseRecognized += KeywordRecognizer_OnPhraseRecognized;
            keywordRecognizer.Start();
        }

        private void KeywordRecognizer_OnPhraseRecognized(PhraseRecognizedEventArgs args)
        {
            System.Action keywordAction;
            if (keywords.TryGetValue(args.text, out keywordAction))
            {
                keywordAction.Invoke();
            }

        }

        public void addKeywords()
        {
            keywords.Add("Move", () => {
                UnityEngine.Debug.Log("Recognized Move command");
                onRotate();
            });

            keywords.Add("Translate", () =>
            {
                UnityEngine.Debug.Log("Recognized Translate command");
                onTranslate();
            });


            keywords.Add("Stop", () =>
            {
                UnityEngine.Debug.Log("Time to stop");
                onStop();

            });

            keywords.Add("Fall", () =>
            {
                UnityEngine.Debug.Log("Adding Gravity");
                onFall();
            });

            keywords.Add("Generate", () =>
            {
                UnityEngine.Debug.Log("Generating Arrows again");
                onGenerate();
            });
            keywords.Add("Increment", () =>
            {
                UnityEngine.Debug.Log("Incrementing the value of n to "+(genArrow.getN()+1).ToString());
                onIncrement();
            });
            keywords.Add("Increase", () =>
            {
                UnityEngine.Debug.Log("Increasing gameObjects by 4 , total number is " + (genArrow.getN() + 4).ToString());
                onIncrease();
            });
        }

        public void onRotate()
        {
            mode = (int)MODES.Rotate;
        }

        public void onTranslate()
        {
            genArrow.first = true;
            mode = (int)MODES.Translate;
        }

        public void onGenerate()
        {
            mode = (int)MODES.Generate;
        }

        public void onFall()
        {
            mode = (int)MODES.Fall;
        }

        public void onStop()
        {
            mode = (int)MODES.Stop;
        }

        public void onIncrement()
        {
            genArrow.first = true;
            mode = (int)MODES.Increment;
        }

        public void onIncrease()
        {
            genArrow.first = true;
            mode = (int)MODES.Increase;
        }

        public int getMode()
        {
            return mode;
        }
        public void setMode(int m)
        {
            mode = m;
        }
    }
}


