﻿#if WINDOWS_UWP
using Windows.Storage;
using System.Threading.Tasks;
using Windows.Data.Xml.Dom;
using System;
#endif

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.Windows.Speech;
using System.IO;
using ArrowCollectionHelper;
using SpeechRecognizerHelper;

public class genArrow : MonoBehaviour {

    // Use this for initialization
    public static int n = 1;

    public enum MODES{Rotate,Translate,Generate,Fall,Stop,Increment, Increase};
    public static int mode;
    public static int count = 0;
    public static int writeCount = 0;
    public static bool first;
    public static float frameRate;
    public static float avg_frameRate = 0;
    public static ArrowCollection arrowCollection;
    public SpeechRecognizer speechRecognizer;
    
    void Start ()
    {
        arrowCollection = new ArrowCollection(getN());
        arrowCollection.location(new Vector3(0, 0, 0));
        speechRecognizer = new SpeechRecognizer();
      
    }

    // Update is called once per frame
    void Update ()
    {
        frameRate = 1 / Time.deltaTime;
        mode = speechRecognizer.getMode();
        //UnityEngine.Debug.Log(mode);
        /*if (mode == (int)MODES.Rotate || mode == (int)MODES.Translate)
        {
            if (writeCount < 10)
            {
                writeCount += 1;
                avg_frameRate += frameRate;


            }
            else if (writeCount == 10) {

                #if WINDOWS_UWP
                    writeToFile(arrowCollection.getN(), mode, avg_frameRate/100);
                #endif
                writeCount += 1;

            }

        }*/

        if (mode == (int)MODES.Rotate)
        {
            arrowCollection.rotate(frameRate);
            
        }
        else if(mode == (int)MODES.Translate)
        {
            if (count > 60 || first == true)
            {
                count = 0;
            }
            arrowCollection.translate(count);
            count += 1;
            first = false;
        }
        else if(mode == (int)MODES.Generate)
        {
            arrowCollection.Generate(new Vector3(0,0,0));
            speechRecognizer.setMode((int)MODES.Stop);
        }
        else if (mode == (int)MODES.Fall)
        {
            arrowCollection.Fall();
            speechRecognizer.setMode((int)MODES.Stop);
        }
        else if (mode == (int)MODES.Increment)
        {
           
           
            if (first == true)
            {
                arrowCollection.destroy();
                setN(n + 1);
                arrowCollection = new ArrowCollection(getN());
                first = false;
                writeCount = 0;
            }
            //speechRecognizer.setMode((int)MODES.Stop);
            else
            {
                if (writeCount <5) {
                    writeCount += 1;
                }
                else if(writeCount >= 5 && writeCount <15)
                {
                    writeCount += 1;
                    avg_frameRate += frameRate;


                }
                else if (writeCount == 15)
                {

                    #if WINDOWS_UWP
                        writeToFile(arrowCollection.getN(), mode, avg_frameRate / 10);
                    #endif
                    writeCount = 0;
                    speechRecognizer.setMode((int)MODES.Stop);

                }
                
            }
        }
        else 
        {
            writeCount = 0;
            avg_frameRate = 0;
            //UnityEngine.Debug.Log("Stopped");
        }

    }

    public static int getN()
    {
        return n;
    }

    public static void setN(int m)
    {
        n = m;
    }

#if WINDOWS_UWP
   
    public async void writeToFile(int n, int mode, float frameRate)
    {
        Dictionary<int, string> Mode = new Dictionary<int, string>();
        Mode.Add(0, "Rotate");
        Mode.Add(1, "Translate");
        Mode.Add(5, "Increment");
        string Path = Directory.GetCurrentDirectory().ToString();
        string fileName = Path + "/readings_new.csv";
        //string data = SystemInfo.deviceName.ToString() + "," + n.ToString() + "," + Mode[mode] + "," + frameRate.ToString() + "\n";
        //File.AppendAllText(fileName, data);
        string data = n.ToString() + "," + frameRate.ToString() + "\n";
      

           try
            {
                StorageFile textFile = await ApplicationData.Current.LocalFolder.GetFileAsync("readings.csv");
                await FileIO.AppendTextAsync(textFile, data);
            }
           catch
            {
                print("File does not exist! Creating file!");
                StorageFile textFile = await ApplicationData.Current.LocalFolder.CreateFileAsync("readings.csv");
                await FileIO.WriteTextAsync(textFile, "n,FrameRate\n");
                await FileIO.AppendTextAsync(textFile, data);
            }
        


}

#endif

}
