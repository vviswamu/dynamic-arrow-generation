﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class frameRate : MonoBehaviour {

    public Text textObject;
    public float lastSaved;
    public float CurrentTime;
    

	// Use this for initialization
	void Start () {
        textObject = GetComponent<Text>();
        lastSaved = Time.time;
	
	}
	
	// Update is called once per frame
	void Update () {
        CurrentTime = Time.time;
        
        if ( CurrentTime - lastSaved > 1)
        {
            if ((1 / Time.deltaTime) > 30)
            {
                textObject.color = Color.white;
            }
            else
            {
                textObject.color = Color.red;
            }

            textObject.text = (1 / Time.deltaTime).ToString("f0");
            
            lastSaved = CurrentTime;
        }
	
	}
}
