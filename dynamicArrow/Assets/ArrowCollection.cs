﻿using UnityEngine;
using System.Collections;
using System;
using SpeechRecognizerHelper;

namespace ArrowCollectionHelper 
{
    public class ArrowCollection : MonoBehaviour
    {
       
        GameObject[,,] arrowArray;
       
        int x;
        int y;
        int z;

        int p;
        int q;
        int r;

        const float distance = 0.05f;
        const float scale = 0.02f;


        public ArrowCollection(int x, int y, int z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
            p = x / 2;
            q = y / 2;
            r = z / 2;
            arrowArray = new GameObject[x, y, z];
            start();
        }


       public void start()
        {
            for (var i = 0; i < x; i++)
            {
                for (var j = 0; j < y; j++)
                {
                    for (var k = 0; k < z; k++)
                    {

                        //arrowArray[i, j, k] = (GameObject)Instantiate(Resources.Load("bowandarrow/Prefabs/arrow"));
                        //arrowArray[i, j, k] = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                        arrowArray[i, j, k] = GameObject.CreatePrimitive(PrimitiveType.Cube);
                        Renderer r = arrowArray[i, j, k].GetComponent<Renderer>();
                        r.material = (Material)Resources.Load("Materials/Red");
                        
                        //render game objects in front of user
                        arrowArray[i, j, k].transform.position = new Vector3((i - p) * distance, ((j - q) * distance), ((k) * distance) + 1.3f);

                        //render game objects around the centre of game objects
                        //arrowArray[i, j, k].transform.position = new Vector3((i - p) * distance, ((j - q) * distance), ((k-r) * distance) );
                        arrowArray[i, j, k].transform.localRotation = new Quaternion(0, 0, 0, 0);
                        arrowArray[i, j, k].transform.localScale = new Vector3(1*scale,1*scale,1*scale);

                        

                    }
                }
            }
            genArrow.mode = (int)genArrow.MODES.Stop;
        }

        public void location(Vector3 initialPosition)
        {
            for (var i = 0; i < x; i++)
            {
                for (var j = 0; j < y; j++)
                {
                    for (var k = 0; k < z; k++)
                    {

                         arrowArray[i, j, k].transform.position = new Vector3((initialPosition.x + i - p) * distance, ((initialPosition.y + j - q) * distance), ((initialPosition.z + k) * distance) + 1.3f);
                        //arrowArray[i, j, k].transform.position = new Vector3((initialPosition.x + i - p) * distance, ((initialPosition.y + j - q) * distance), ((initialPosition.z + k - r) * distance));

                    }
                }
            }

        }

        

        public void rotate(float frameRate)
        {
            for (var i = 0; i < x; i++)
            {
                for (var j = 0; j < y; j++)
                {

                    for (var k = 0; k < z; k++)
                    {

                        arrowArray[i, j, k].transform.Rotate(0, (360 / frameRate), 0);

                    }
                }
            }
        }


        public void translate(int l)
        {

            var radius = 0.01f;

            for (var i = 0; i < x; i++)
            {
                for (var j = 0; j < y; j++)
                {
                    for (var k = 0; k < z; k++)
                    {
                        arrowArray[i, j, k].transform.position += getNextPosition(l * 6, radius, new Vector3(0, 0, 0));
                        arrowArray[i, j, k].transform.Rotate(0, 6, 0);
                    }
                }
            }

        }

        Vector3 getNextPosition(float angle, float radius, Vector3 center)
        {

            Vector3 resultPosition = new Vector3();
            resultPosition.x = center.x + radius * Mathf.Sin(angle * Mathf.Deg2Rad);
            resultPosition.z = center.z + radius * Mathf.Cos(angle * Mathf.Deg2Rad);
            resultPosition.y = center.y;
            return resultPosition;
        }

        public void Generate(Vector3 initialPosition)
        {
            float[,,] lengths = new float[x,y,z];
            System.Random rand = new System.Random();

            for (var i = 0; i < x; i++)
            {
                for (var j = 0; j < y; j++)
                {

                    for (var k = 0; k < z; k++)
                    {
                        Rigidbody gameObjectsRigidBody = arrowArray[i, j, k].GetComponent<Rigidbody>();
                        Destroy(gameObjectsRigidBody);

                        //generate game objects in front of user
                        //arrowArray[i, j, k].transform.position = new Vector3((initialPosition.x + i - p) * distance, (initialPosition.y + j - q) * distance, ((initialPosition.z + k - r) * distance));

                        //generate game objects around user
                        arrowArray[i, j, k].transform.position = new Vector3((initialPosition.x + i - p) * distance, (initialPosition.y + j - q) * distance, ((initialPosition.z + k ) * distance) + 1.3f);
                        
                        //generate objects with fixed scale
                        //arrowArray[i, j, k].transform.localScale = new Vector3(1*scale, 1*scale, 1*scale);

                        //generate objects with random size... Here we can pass an array of float values to render objects of required size
                        arrowArray[i, j, k].transform.localScale = new Vector3(rand.Next(1,5) * scale, rand.Next(1,5) * scale, rand.Next(1,5) * scale);

                        //generate objects with random rotation... Here we can pass an array of float values to render objects of required rotation
                        arrowArray[i, j, k].transform.Rotate(rand.Next(-360,360), rand.Next(-360,360), rand.Next(-360,360));
                        
                    }
                }
            }
            
        }

        public void Fall()
        {
            for (var i = 0; i < x; i++)
            {
                for (var j = 0; j < y; j++)
                {

                    for (var k = 0; k < z; k++)
                    {

                        Rigidbody gameObjectsRigidBody = arrowArray[i, j, k].AddComponent<Rigidbody>();

                    }
                }
            }
 
        }

        public void destroy()
        {
            for (var i = 0; i < x; i++)
            {
                for (var j = 0; j < y; j++)
                {

                    for (var k = 0; k < z; k++)
                    {

                        Destroy(arrowArray[i, j, k]);

                    }
                }
            }

        }

    

    }
}
