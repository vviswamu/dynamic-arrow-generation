﻿#if WINDOWS_UWP
using Windows.Storage;
using System.Threading.Tasks;
using Windows.Data.Xml.Dom;
using System;
#endif

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.Windows.Speech;
using System.IO;
using ArrowCollectionHelper;
using SpeechRecognizerHelper;

public class genArrow : MonoBehaviour {

    // Use this for initialization
    
    public static int x = 12;
    public static int y = 20;
    public static int z = 2;

    public enum MODES{Rotate,Translate,Generate,Fall,Stop,Increment, Increase};
    public static int mode;
    public static int count = 0;
    public static int writeCount = 0;
    public static bool first;
    public static float frameRate;
    public static float avg_frameRate = 0;
    public static ArrowCollection arrowCollection;
    public SpeechRecognizer speechRecognizer;
    
    void Start ()
    {
        arrowCollection = new ArrowCollection(getX(),getY(),getZ());
        arrowCollection.location(new Vector3(0, 0, 0));
        speechRecognizer = new SpeechRecognizer();
    }

    // Update is called once per frame
    void Update ()
    {
        frameRate = 1 / Time.deltaTime;
        mode = speechRecognizer.getMode();

        //UnityEngine.Debug.Log(mode);

        //Write Frame Rate  into file if mode is rotate or translate
        /*if (mode == (int)MODES.Rotate || mode == (int)MODES.Translate)
        {
            if (writeCount < 10)
            {
                writeCount += 1;
                avg_frameRate += frameRate;


            }
            else if (writeCount == 10) {

                #if WINDOWS_UWP
                    writeToFile((arrowCollection.getX()*arrowCollection.getY()*arrowCollection.getZ()), mode, avg_frameRate/10);
                #endif
                writeCount += 1;

            }

        }*/



        if (mode == (int)MODES.Rotate)
        {
            arrowCollection.rotate(frameRate);
            
        }
        else if(mode == (int)MODES.Translate)
        {
            if (count > 60 || first == true)
            {
                count = 0;
            }
            arrowCollection.translate(count);
            count += 1;
            first = false;
        }
        else if(mode == (int)MODES.Generate)
        {
            arrowCollection.Generate(new Vector3(0,0,0));
            speechRecognizer.setMode((int)MODES.Stop);
        }
        else if (mode == (int)MODES.Fall)
        {
            arrowCollection.Fall();
            speechRecognizer.setMode((int)MODES.Stop);
        }
        else if (mode == (int)MODES.Increment)
        {
           
           
            if (first == true)
            {
                arrowCollection.destroy();
                
                //increment in either x, z or z direction
                setX(x + 1);
                //setY(y + 1);
                //setZ(z + 1);
                arrowCollection = new ArrowCollection(getX(),getY(),getZ());


                first = false;
                writeCount = 0;
            }
            
            else
            {
                if (writeCount <5) {
                    writeCount += 1;
                }
                else if(writeCount >= 5 && writeCount <15)
                {
                    writeCount += 1;
                    avg_frameRate += frameRate;


                }
                else if (writeCount == 15)
                {

                    #if WINDOWS_UWP
                        writeToFile(x*y*z, mode, avg_frameRate / 10);
                    #endif
                    writeCount = 0;
                    speechRecognizer.setMode((int)MODES.Stop);

                }
                
            }
        }
        else 
        {
            writeCount = 0;
            avg_frameRate = 0;
            //UnityEngine.Debug.Log("Stopped");
        }


        

    }

   
    public static int getX()
    {
        return x;
    }

    public static void setX(int x)
    {
        genArrow.x = x;
    }

    public static int getY()
    {
        return y;
    }

    public static void setY(int y)
    {
        genArrow.y = y;
    }

    public static int getZ()
    {
        return z;
    }

    public static void setZ(int z)
    {
        genArrow.z = z;
    }
    

#if WINDOWS_UWP
   
    public async void writeToFile(int n, int mode, float frameRate)
    {
        Dictionary<int, string> Mode = new Dictionary<int, string>();
        Mode.Add(0, "Rotate");
        Mode.Add(1, "Translate");
        Mode.Add(5, "Increment");
        string Path = Directory.GetCurrentDirectory().ToString();
        string fileName = Path + "/readings_new.csv";

        //To append mode with frameRate in File
        //string data = SystemInfo.deviceName.ToString() + "," + n.ToString() + "," + Mode[mode] + "," + frameRate.ToString() + "\n";
        //File.AppendAllText(fileName, data);
        //string data = n.ToString() + "," + frameRate.ToString() + "\n";

        string data = (x*y*z).ToString() + "," + frameRate.ToString() + "\n";
      

           try
            {
                StorageFile textFile = await ApplicationData.Current.LocalFolder.GetFileAsync("readings.csv");
                await FileIO.AppendTextAsync(textFile, data);
            }
           catch
            {
                print("File does not exist! Creating file!");
                StorageFile textFile = await ApplicationData.Current.LocalFolder.CreateFileAsync("readings.csv");
                await FileIO.WriteTextAsync(textFile, "n,FrameRate\n");
                await FileIO.AppendTextAsync(textFile, data);
            }
        


}

#endif

}
